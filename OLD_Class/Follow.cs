using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace M2Link.Models
{
    public class Follow
    {
        [KeyAttribute]
        public virtual Guid guid { get; set; }

        public string user { get; set; }

        public string followed_user { get; set; }

        public Follow()
        {
        }

        public Follow(string user, string followed_user)
        {
            this.guid = Guid.NewGuid();
            this.user = user;
            this.followed_user = followed_user;
        }

        public Follow(Guid guid, string user, string followed_user)
        {
            this.guid = guid;
            this.user = user;
            this.followed_user = followed_user;
        }
    }
}