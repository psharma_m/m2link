﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M2Link.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            //Setup du client
            ExternalWebService1.WSLoginSoapClient clientLogin = new ExternalWebService1.WSLoginSoapClient();

            //Entrée du nickname + password
            System.Console.WriteLine("Entrez le nom de l'utilisateur :");
            string nickname = System.Console.ReadLine();
            System.Console.WriteLine("Entrez le mot de passe :");
            string password = System.Console.ReadLine();

            ExternalWebService1.UserModel userModel = clientLogin.Validate(nickname, password);
            if(userModel == null)
            {
                System.Console.WriteLine("* Utilisateur non valide *");
            }
            else
            {
                System.Console.WriteLine("* Utilisateur valide *");

                //Setup du client des messages
                ExternalWebService.WSMessageSoapClient clientMessage = new ExternalWebService.WSMessageSoapClient();

                ExternalWebService.MessageModel[] listMsg = clientMessage.get_followed_users_messages(nickname);

                System.Console.WriteLine("---");
                System.Console.WriteLine("Liste des messages que cet utilisateur suit :");
                foreach (ExternalWebService.MessageModel msg in listMsg)
                {
                    System.Console.WriteLine("{" + msg.author + "}(" + msg.date + ") " + msg.message);
                }

            }

            System.Console.ReadLine();
        }
    }
}
