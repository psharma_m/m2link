﻿using M2Link.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace M2Link.Context
{
    public class M2LinkInitializer : DropCreateDatabaseIfModelChanges<M2LinkContext>
    {
        public M2LinkInitializer() {}

        protected override void Seed(M2LinkContext context)
        {
            UserRepository repo = new UserRepository(context);
            if (repo.getUser("Alexandre") == null)
            {
                Entities.User user = new Entities.User()
                {
                    first_name = "HAZARD",
                    last_name = "Alexandre",
                    email = "hazard.alexandre76@gmail.com",
                    nickname = "Alexandre",
                    password = "alex_123"
                };
                List<Entities.User> followed_base = new List<Entities.User>();
                followed_base.Add(user);
                user.followed = followed_base;

                MessageRepository repoMsg = new MessageRepository(context);
                repoMsg.Add(new Entities.Message()
                {
                    author = user,
                    message = "Bienvenue sur mon application .NET !"
                });

                repo.Add(user);
                context.SaveChanges();
            }
        }
    }
}