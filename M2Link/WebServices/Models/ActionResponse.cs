﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace M2Link.WebServices.Models
{
    public class ActionResponse
    {
        public int code { get; set; }

        public string message { get; set; }

        public ActionResponse() {}

        public ActionResponse(int code, string message)
        {
            this.code = code;
            this.message = message;
        }
    }
}