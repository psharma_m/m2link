﻿using M2Link.Context;
using M2Link.Entities;
using M2Link.Models;
using M2Link.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace M2Link.Controllers
{
    [Authorize]
    public class ProfileController : Controller
    {
        // GET: User Index
        public ActionResult Index()
        {
            //Si l'on a plus les informations du User dans la Database
            using (M2LinkContext context = new M2LinkContext())
            {
                UserRepository repo = new UserRepository(context);
                User user_base = repo.getUser(HttpContext.User.Identity.Name);
                UserModel user = new UserModel()
                {
                    email = user_base.email,
                    first_name = user_base.first_name,
                    last_name = user_base.last_name,
                    nickname = user_base.nickname
                };
                if (user == null)
                {
                    FormsAuthentication.SignOut();
                    return RedirectToAction("Index", "Home");
                } else
                {
                    MessageRepository msgRepo = new MessageRepository(context);
                    List<MessageModel> messages = new List<MessageModel>();
                    foreach (Message msg in user_base.messages)
                    {
                        MessageModel msgModel = new MessageModel()
                        {
                            guid = msg.guid,
                            author = msg.author.nickname,
                            date = msg.date,
                            message = msg.message
                        };
                        messages.Add(msgModel);
                    }

                    //--Construct followed_users nicknames
                    List<string> followed_users = new List<string>();
                    foreach (User u in user_base.followed)
                    {
                        followed_users.Add(u.nickname);
                    }
                    //--
                    ProfileModel profile_user = new ProfileModel()
                    {
                        email = user.email,
                        first_name = user.first_name,
                        last_name = user.last_name,
                        nickname = user.nickname,
                        messages = messages,
                        followed_users = followed_users
                    };
                    return View(profile_user);
                }
            }
        }

        // GET: Edit
        public ActionResult Edit()
        {
            //Si l'on a plus les informations du User dans la Database
            using (M2LinkContext context = new M2LinkContext())
            {
                UserRepository repo = new UserRepository(context);
                User user_base = repo.getUser(HttpContext.User.Identity.Name);
                UserModel user = new UserModel()
                {
                    email = user_base.email,
                    first_name = user_base.first_name,
                    last_name = user_base.last_name,
                    nickname = user_base.nickname
                };
                if (user == null)
                {
                    FormsAuthentication.SignOut();
                    return RedirectToAction("Index", "Home");
                }
                ProfileModel profile_user = new ProfileModel()
                {
                    email = user.email,
                    first_name = user.first_name,
                    last_name = user.last_name,
                    nickname = user.nickname,
                };
                return View(profile_user);
            }
        }

        // POST: Edit
        [HttpPost]
        public ActionResult Edit(ProfileModel profile)
        {
            //Si l'on a plus les informations du User dans la Database
            using (M2LinkContext context = new M2LinkContext())
            {
                UserRepository repo = new UserRepository(context);
                User user = repo.getUser(HttpContext.User.Identity.Name);
                if (user == null)
                {
                    FormsAuthentication.SignOut();
                    return RedirectToAction("Index", "Home");
                }
                repo.EditUser(HttpContext.User.Identity.Name, profile);
                context.SaveChanges();
                return RedirectToAction("Index", "Profile");
            }
        }

        // GET: Edit
        public ActionResult ChangePassword()
        {
            return View();
        }

        // POST: Edit
        [HttpPost]
        public ActionResult ChangePassword(ProfilePasswordModel model)
        {
            //Si l'on a plus les informations du User dans la Database
            using (M2LinkContext context = new M2LinkContext())
            {
                UserRepository repo = new UserRepository(context);
                User user = repo.getUser(HttpContext.User.Identity.Name);
                if (user == null)
                {
                    FormsAuthentication.SignOut();
                    return RedirectToAction("Index", "Home");
                }
                repo.changePassword(HttpContext.User.Identity.Name, model.password);
                context.SaveChanges();
                return RedirectToAction("Index", "Profile");
            }
        }

        // GET: List
        public ActionResult List()
        {
            using (M2LinkContext context = new M2LinkContext())
            {
                UserRepository repo = new UserRepository(context);

                var profiles = repo.GetAll().Select(u => new ProfileModel()
                {
                    email = u.email,
                    first_name = u.last_name,
                    last_name = u.last_name,
                    nickname = u.nickname
                }).ToList();

                //Reconstitution des personnes suivies par l'utilisateur connecté
                ProfileModel p_user = profiles.Find(p => p.nickname == HttpContext.User.Identity.Name);
                foreach (string s in repo.getFollowedUsersNicknames(HttpContext.User.Identity.Name))
                {
                    p_user.followed_users.Add(s);
                }

                return View(profiles);
            }
        }

        // GET: Follow
        public ActionResult Follow(string nickname, string source)
        {
            using (M2LinkContext context = new M2LinkContext())
            {
                UserRepository repo = new UserRepository(context);
                repo.Follow(HttpContext.User.Identity.Name, nickname);
                context.SaveChanges();
            }
            return Redirect(source);

            //return RedirectToAction("List", "Profile");
        }

        // GET: Follow
        public ActionResult Unfollow(string nickname, string source)
        {
            using (M2LinkContext context = new M2LinkContext())
            {
                UserRepository repo = new UserRepository(context);
                repo.Unfollow(HttpContext.User.Identity.Name, nickname);
                context.SaveChanges();
            }
            return Redirect(source);

            //return RedirectToAction("List", "Profile");
        }

    }
}