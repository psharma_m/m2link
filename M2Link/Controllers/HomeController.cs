﻿using M2Link.Context;
using M2Link.Entities;
using M2Link.Models;
using M2Link.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace M2Link.Controllers
{
    
    public class HomeController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
            using (M2LinkContext context = new M2LinkContext())
            {
                MessageRepository msgRepo = new MessageRepository(context);
                List<MessageModel> messages = new List<MessageModel>();

                //FollowRepository followRepo = new FollowRepository(context);
                //List<string> list_follow = followRepo.getFollowedUsersNickname(HttpContext.User.Identity.Name);

                UserRepository userRepo = new UserRepository(context);
                List<string> list_follow = userRepo.getFollowedUsersNicknames(HttpContext.User.Identity.Name);

                foreach (Message msg in msgRepo.getMessages(list_follow))
                {
                    MessageModel msgModel = new MessageModel()
                    {
                        guid = msg.guid,
                        author = msg.author.nickname,
                        date = msg.date,
                        message = msg.message
                    };
                    messages.Add(msgModel);
                }
                return View(messages);
            }
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

    }
}