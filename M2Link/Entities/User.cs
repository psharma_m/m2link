using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace M2Link.Entities
{
    public class User
    {
        [KeyAttribute]
        public virtual Guid guid { get; set; }

        public string last_name { get; set; }

        public string first_name { get; set; }

        public string email { get; set; }

        public string nickname { get; set; }

        public string password { get; set; }

        public virtual List<User> followed { get; set; }

        //Unused (only for join table creation)
        public virtual List<User> followed_by { get; set; }

        public virtual List<Message> messages { get; set; }

        public User()
        {
            this.guid = Guid.NewGuid();
            this.followed = new List<User>();
            this.followed_by = new List<User>();
            this.messages = new List<Message>();
        }
    }
}