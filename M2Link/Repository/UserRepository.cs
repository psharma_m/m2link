﻿using M2Link.Context;
using M2Link.Entities;
using M2Link.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace M2Link
{
    public class UserRepository
    {
        public M2LinkContext context { get; set; }

        public UserRepository(M2LinkContext context)
        {
            this.context = context;
        }

        public void Add(User user)
        {
            context.Users.Add(user);
        }

        public List<User> GetAll()
        {
            return context.Users.ToList();
        }

        public Boolean checkUser(string nickname, string password) 
        {
            return context.Users.Any(user => user.nickname == nickname && user.password == password);
        }

        public User getUser(string nickname)
        {
            return context.Users.SingleOrDefault(user => user.nickname == nickname);
        }

        public void EditUser(string nickname, ProfileModel profile)
        {
            User entityUser = getUser(nickname);
            entityUser.nickname = profile.nickname;
            entityUser.first_name = profile.first_name;
            entityUser.last_name = profile.last_name;
            entityUser.email = profile.email;
        }

        public void changePassword(string nickname, string password)
        {
            User entityUser = getUser(nickname);
            entityUser.password = password;
        }


        //****** Follow feature ******

        public List<User> getFollowedUsers(string nickname)
        {
            return getFollowedUsers(getUser(nickname)).ToList();
        }

        public List<User> getFollowedUsers(User user)
        {
            if (user == null) return new List<User>();
            return user.followed.ToList();
        }

        public List<string> getFollowedUsersNicknames(string nickname)
        {
            return getFollowedUsersNicknames(getUser(nickname));
        }

        public List<string> getFollowedUsersNicknames(User user)
        {
            List<string> nicknames = new List<string>();
            if (user == null) return nicknames;
            foreach (var item in user.followed)
            {
                nicknames.Add(item.nickname);
            }
            return nicknames;
        }

        public void Follow(string nickname, string nickname_to_follow)
        {
            User user = getUser(nickname);
            User user_to_follow = getUser(nickname_to_follow);
            if (!user.followed.Contains(user_to_follow))
            {
                user.followed.Add(user_to_follow);
            }
        }

        public void Unfollow(string nickname, string nickname_to_unfollow)
        {
            User user = getUser(nickname);
            User user_to_unfollow = getUser(nickname_to_unfollow);
            if (user.followed.Contains(user_to_unfollow))
            {
                user.followed.Remove(user_to_unfollow);
            }
        }
    }
}