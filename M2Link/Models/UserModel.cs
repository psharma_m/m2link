﻿using M2Link.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace M2Link.Models
{
    public class UserModel
    {
        [Required]
        [Display(Name = "Name")]
        public string last_name { get; set; }

        [Required]
        [Display(Name = "Prenom")]
        public string first_name { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Adresse email")]
        public string email { get; set; }

        [Required]
        [Display(Name = "Pseudo")]
        public string nickname { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Mot de passe")]
        public string password { get; set; }

        public UserModel() { }

        public UserModel(User user)
        {
            this.last_name = user.last_name;
            this.first_name = user.first_name;
            this.email = user.email;
            this.nickname = user.nickname;
            this.password = user.password;
        }

        public UserModel(string last_name, string first_name, string email, string nickname, string password)
        {
            this.last_name = last_name;
            this.first_name = first_name;
            this.email = email;
            this.nickname = nickname;
            this.password = password;
        }
    }
}