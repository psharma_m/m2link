﻿using System.ComponentModel.DataAnnotations;

namespace M2Link.Models
{
    public class LoginModel
    {
        [Required]
        [Display(Name = "Pseudo")]
        public string nickname { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Mot de passe")]
        public string password { get; set; }

        public LoginModel() {}

        public LoginModel(string nickname, string password)
        {
            this.nickname = nickname;
            this.password = password;
        }
    }
}