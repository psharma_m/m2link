﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using M2Link.Entities;

namespace M2Link.Models
{
    public class ProfilePasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Mot de passe")]
        public string password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Verif. du mot de passe")]
        [Compare("password")]
        public string verify_password { get; set; }

        public ProfilePasswordModel()
        {
        }

        public ProfilePasswordModel(string password, string verify_password)
        {
            this.password = password;
            this.verify_password = verify_password;
        }

        public ProfilePasswordModel(User user)
        {
            this.password = user.password;
            this.verify_password = user.password;
        }
    }
}